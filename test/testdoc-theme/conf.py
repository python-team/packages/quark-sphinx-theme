import quark_sphinx_theme

master_doc = 'contents'
html_theme_path = [quark_sphinx_theme.get_path()]
html_theme = 'quark'
html_split_index = True
