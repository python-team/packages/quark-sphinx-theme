{#- Template variable defintions -#}
{% set text_color = theme_text_color or '#000000' -%}
{% set link_color = theme_link_color or '#0000ff' -%}


body {
    color: {{ text_color }};

    {% if theme_body_font -%}
        font-family: {{ theme_body_font }};
    {%- endif %}

    {% if theme_body_font_size -%}
        font-size: {{ theme_body_font_size }};
    {%- endif %}
}

.document {
    margin-left: 2em;
    margin-right: 2em;
}

li {
    margin-top: 0.3em;
    margin-bottom: 0.3em;
    text-indent: {{ theme_li_extra_indent }}px;
}

{% for x in range(2, 10) -%}
{{ 'li ' * x }}{
    text-indent: {{ theme_li_extra_indent | int * x }}px;
}
{% endfor %}

blockquote {
    text-indent: {{ theme_li_extra_indent }}px;
}

/* Definition lists */
dt {
    font-weight: bold;
}

dd > * {
    margin-top: 0;
}

li > dl > dd {
    margin-left: 0;
}

/* Compact list produced by hlist:: directive */
table.hlist {
    margin-top: 1em;
}

table.hlist ul {
    margin-top: 0;
    margin-bottom: 0;
}



/* Header/footer navigation bar *
 * ---------------------------- */
.navbar {
    background-color: {{ theme_navbar_bgcolor or 'transparent' }};

    width: 100%;
}

{% if theme_navbar_color -%}
.navbar * {
    color: {{ theme_navbar_color }};
}
{%- endif %}

.navbar a {
    {% if theme_underline_navbar_links -%}
    text-decoration: underline;
    {% endif %}
}

.navbar td {
    padding: 0.2em;
}

.navbar-bottom {
    margin-top: 2em;
}

.footer {
    font-style: italic;
    font-size: small;
}



/* Nav sidebar *
 * ----------- */
.nav-sidebar {
    margin: 2em;
    background-color: {{ theme_sidebar_bgcolor or 'transparent' }};
    float: right;

    {% if theme_sidebar_border -%}
    border-style: solid;
    border-width: 1px;
    {%- endif %}

    border-color: {{ text_color }};

    width: 20%;
}

.nav-sidebar .nav-sidebar-td {
    padding: 1em;
}

{% for i in range(1, 7) -%}
.sphinxsidebar h{{ i }} {
    font-size: medium;
    font-weight: bold;
}
{% endfor %}



/* Special index page markup *
 * ------------------------- */
.genindextable td {
    padding-top: 1em;
    padding-bottom: 1em;
}

.genindextable dl {
    margin: 0;
}



/* Links *
 * ----- */
a {
    color: {{ link_color }};
    text-decoration: {% if theme_underline_links -%}
        underline
    {%- else -%}
        none
    {%- endif %};
}

a.headerlink {
    color: {{ theme_headerlink_color or link_color }};
    text-decoration: none;
    font-size: medium;
    font-weight: bold;

    padding: 0.2em;
}

/* Object references */
a code.xref {
    font-weight: bold;
    color: {{ theme_xref_color or link_color }};
    background-color: {{ theme_code_bgcolor or 'transparent' }};
    text-decoration: none;
}

/* Footnote references */
a.footnote-reference {
    font-size: small;
}



/* Code listings *
 * ------------- */
code, pre {
    {% if theme_code_font -%}
    font-family: {{ theme_code_font }};
    {%- endif %}

    {% if theme_code_font_size -%}
    font-size: {{ theme_code_font_size }};
    {%- endif %}
}

code {
    background-color: {{ theme_code_bgcolor or 'transparent' }};
}

code.descname {
    font-weight: bold;
    background-color: transparent;
}

code.descclassname {
    background-color: transparent;
}

pre {
    margin: 0.5em;
    margin-top: 1.5em;

    {% if theme_pre_bgcolor -%}
    background-color: {{ theme_pre_bgcolor }} !important;
    {%- endif %}
}

/* Extra rules for literal blocks wrapped by ``html_rewrite`` */
.-x-quark-literal-block {
    margin-top: 1em;
    width: 100%;
}

.-x-quark-literal-block .-x-quark-literal-block-td {
    /* Use td for padding on QTextBrowser... */
    padding: 0.5em;
}
.-x-quark-literal-block .-x-quark-literal-block-td:only-child {
    /* ...disable it again on full browsers... */
    padding: 0;
}
.-x-quark-literal-block-td pre {
    margin: 0;
    /* ...and put the padding on the pre instead. */
    padding: 0.5em;
}


/* Code blocks in headings and general links */
h1 code, h2 code, h3 code, h4 code, h5 code, h6 code {
    background-color: transparent;
}

a code {
    background-color: transparent;
}



/* Regular tables *
 * -------------- */
table.docutils {
    margin-top: 2em;
}

table.docutils td, table.docutils th {
    padding: 0.3em;
}



/* Footnotes and citations *
 * ----------------------- */
table.docutils.footnote, table.docutils.citation {
    margin: 0;
}

table.docutils.footnote td, table.docutils.citation td {
    padding: 0;
}

table.docutils.footnote td.label, table.docutils.citation td.label {
    padding-right: 2em;
}



/* Boxes -- admonitions, sidebars, topics *
 * -------------------------------------- */

/* This section has good styles for the improved boxes created by
 * ``html_rewrite`` (the 'boxes') feature and some fallback styles if those are
 * disabled.
 */

/* Part 1 -- common styles */
.admonition-title, .sidebar-title, .topic-title {
    font-weight: bold;
}

.sidebar-subtitle {
    font-style: italic;
}

.admonition > .admonition-title {
    color: {{ theme_admonition_fgcolor or text_color }};
}

.admonition.warning > .admonition-title {
    color: {{ theme_warning_fgcolor or text_color }};
}


/* Part 2 -- fallback-specific styles */
.admonition > *, .topic > *, .sidebar > * {
    margin-left: 3em;
    margin-right: 5em;
}

.admonition > .first, .topic > .first, .sidebar > .first {
    margin-top: 3em;
    margin-left: 0;
    margin-right: 5em;
}


/* Part 3 -- x-quark-box styles */
.-x-quark-box .first {
    margin-top: 0;
    margin-left: 0;
    margin-right: 0;
}

.-x-quark-box .-x-quark-box-td > div > .last, .-x-quark-box .topic > :last-child {
    margin-bottom: 0;
}

.-x-quark-box .-x-quark-box-td > div > * {
    margin-left: 0;
    margin-right: 0;
}

.-x-quark-box {
    margin-top: 2em;
}

.-x-quark-box .-x-quark-box-td {
    padding: 0.5em;
}

.-x-quark-topic, .-x-quark-sidebar {
    border-width: 1px;
    border-style: solid;
    border-color: {{ text_color }};
}

.-x-quark-topic {
    width: 100%;
}

.-x-quark-sidebar {
    margin-left: 1em;
    float: right;

    width: 35%;
}

.-x-quark-admonition {
    background-color: {{ theme_admonition_bgcolor or 'transparent' }};

    {% if theme_admonition_border -%}
    border-style: solid;
    border-width: 1px;
    {%- endif %}

    border-color: {{ theme_admonition_fgcolor or text_color }};

    width: 100%;
}

.-x-quark-admonition.-x-quark-warning {
    background-color: {{ theme_warning_bgcolor or 'transparent' }};
    border-color: {{ theme_warning_fgcolor or text_color }};
}



/* Misc markup *
 * ----------- */
.rubric {
    font-weight: bold;
}

.versionmodified {
    font-style: italic;
}

.field-list {
    margin-top: 0;
}

.guilabel, .menuselection {
    background-color: {{ theme_code_bgcolor or 'transparent' }};
}

.accelerator {
    text-decoration: underline;
}

.caption-text {
    font-weight: bold;
}

div.line-block > div.line {
    margin-top: 0;
    margin-bottom: 0;
}



{% if theme_full_browser_extras -%}

/* Some extra niceties for full browsers *
 * ------------------------------------------- */
a:hover {
    text-decoration: underline;
}

a.headerlink:hover {
    color: {{ link_color }};
    text-decoration: none;
}

.navbar a:hover {
    color: {{ link_color }};
}

pre:only-child {
    /* This gets us nicer literal blocks even without html_rewrite */
    margin: 0;
    margin-top: 1em;
    padding: 0.5em;
}

{%- endif %}
